import UIKit

extension UIViewController {

    func showAlertView(title: String?, message: String?, firstButton: String?, otherButtons: [String]? = nil,
                       type: UIAlertController.Style = .alert, cancelAction: (() -> Void)? = nil,
                       otherAction: ((Int) -> Void)? = nil) {
        let alertViewController = UIAlertController(title: title ?? "Attention" ,
                                                    message: message,
                                                    preferredStyle: .alert)

        if let cancelButton = firstButton {
            let cancelAction = UIAlertAction(title: cancelButton, style: .cancel, handler: { (_) in
                cancelAction?()
            })
            alertViewController.addAction(cancelAction)
        }

        if let otherButtons = otherButtons {
            for (index, otherButton) in otherButtons.enumerated() {
                let otherAction = UIAlertAction(title: otherButton,
                                                style: .default, handler: { (_) in
                                                    otherAction?(index)
                })
                alertViewController.addAction(otherAction)
            }
        }
        DispatchQueue.main.async {
            self.present(alertViewController, animated: true, completion: nil)
        }
    }

    func showErrorAlert(message: String?) {
        showAlertView(title: "Error", message: message, firstButton: "OK")
    }
}
