//
//  AppConstants.swift
//  ProjectTestV3
//
//  Created by admin on 15/10/2021.
//

import Foundation

class AppConstants {
    static let endPoint = "https://tapuniverse.com"
    static let projectsListFileName = "projectsList.json"
}
