//
//  ProjectTableViewCell.swift
//  ProjectTestV3
//
//  Created by admin on 14/10/2021.
//

import UIKit
protocol ProjectTableViewCellDelegate: AnyObject {
    func deleteCell(_ cell: ProjectTableViewCell)
}

class ProjectTableViewCell: UITableViewCell {
    var isCellDidLoad: Bool = false
    weak var delegate: ProjectTableViewCellDelegate?
    @IBOutlet weak var projectNameLabel: UILabel!
    
    @IBOutlet weak var projectNameViewTrailingConstraint: NSLayoutConstraint!
    var beginX: CGFloat = 0
    
    var myBeginX: CGFloat = 0
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func draw(_ rect: CGRect) {
        if isCellDidLoad {return}
        projectNameViewTrailingConstraint.constant = 0
        addGestureRecognizer(UIPanGestureRecognizer(target: self, action: #selector(didPan(_:))))
        isCellDidLoad = true
    }
    
    @objc func didPan(_ sender: UIPanGestureRecognizer) {
        if sender.state == .began {
//             beginX = trailing.constant
            beginX = projectNameViewTrailingConstraint.constant
          
        }
        if sender.state == .changed {
//            trailing.constant = beginX + g.translation(in slef).x
            
            let pannedDistance = sender.translation(in: self).x
            print("change   \(pannedDistance)")
            
            projectNameViewTrailingConstraint.constant = beginX - pannedDistance
            
            print("trail  \( projectNameViewTrailingConstraint.constant)")
        }
        
        if sender.state == .ended || sender.state == .cancelled {
            var constant = projectNameViewTrailingConstraint.constant
            print(constant)
            constant = min(50, max(0, constant))
            constant = constant < 25 ? 0 : 50
            projectNameViewTrailingConstraint.constant = constant
            UIView.animate(withDuration: 0.2) {
                self.layoutIfNeeded()
            }
        }
        /// 2 cases
        /// 0, 50
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configData(_ project: Project) {
        projectNameLabel.text = project.name
    }
    @IBAction func deleteCell(_ sender: Any) {
        if let d = delegate {
            d.deleteCell(self)
        }
    }
}
