//
//  ViewController.swift
//  ProjectTestV3
//
//  Created by admin on 14/10/2021.
//

import UIKit

class Screen1ViewController: UIViewController {
    
    @IBOutlet weak var projectsTableView: UITableView!
    var model = ProjectsData.shared
    
    override func viewDidLoad() {
        super.viewDidLoad()
        projectsTableView.delegate = self
        projectsTableView.dataSource = self
        NotificationCenter.default.addObserver(forName: NSNotification.Name("didFinishGetListProject"), object: nil, queue: nil) { _ in
            self.projectsTableView.reloadData()
        }
    }
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    @IBAction func addProject(_ sender: Any) {
        let alert = UIAlertController(title: "Add project", message: "Enter project name", preferredStyle: .alert)
        alert.addTextField { (textField) in
            textField.placeholder = "Your project name here"
        }
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { [weak alert] (_) in
            let textField = alert?.textFields![0]
            if (textField?.text != "") {
                let project = Project(id: UUID().uuidString, name: (textField?.text!)!)
                self.model.projects.append(project)
                self.model.saveProjectsList()
                self.projectsTableView.reloadData()
            } else {
                let emptyTextAlert = UIAlertController(title: "Empty text", message: "Please don't leave name blank", preferredStyle: .alert)
                emptyTextAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                self.present(emptyTextAlert, animated: true, completion: nil)
            }
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
}

extension Screen1ViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return model.projects.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = projectsTableView.dequeueReusableCell(withIdentifier: "ProjectTableViewCell") as? ProjectTableViewCell else {
            return UITableViewCell()
            
        }
        cell.delegate = self
        cell.configData(model.projects[indexPath.row])
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Screen2", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "Screen2ViewController") as! Screen2ViewController
        vc.project = model.projects[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension Screen1ViewController: ProjectTableViewCellDelegate {
    func deleteCell(_ cell: ProjectTableViewCell) {
        if let indexPath = projectsTableView.indexPath(for: cell) {
            model.projects.remove(at: indexPath.row)
            projectsTableView.reloadData()
        }
    }
}

