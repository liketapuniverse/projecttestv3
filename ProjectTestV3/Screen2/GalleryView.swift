//
//  GalleryView.swift
//  photoProjectV2
//
//  Created by admin on 13/10/2021.
//

import UIKit

// protocol

protocol GalleryViewDelegate: AnyObject {
    func photoSelected(_ view: UIView)
    
}


class GalleryView: UIView {
    
    private var project = Project(id: "-1", name: "n/a")
    private var imageViews: [CustomImageView] = []
    var selected: Int?
    weak var galleryViewDelegate: GalleryViewDelegate?
    
    weak var editViewDelegate: EditViewDelegate? // to remove border layer
    
    let deleteButton = UIButton()
    
    var didLoad = false
    let url = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
    
    override func draw(_ rect: CGRect) {
        // khi view moi hien
        // khi setNeedsDisplay
        
        if didLoad {return}
        didLoad = true
        
        let buttonSize: Int = 20
        deleteButton.frame = CGRect(x: 0, y: 0, width: buttonSize, height: buttonSize)
        deleteButton.layer.cornerRadius = CGFloat(buttonSize/2)
        deleteButton.backgroundColor = .red
        let centerWhiteView = UIView()
        centerWhiteView.frame = CGRect(x: 0, y: 0, width: 8, height: 2)
        centerWhiteView.backgroundColor = .white
        centerWhiteView.center = deleteButton.convert(deleteButton.center, from:deleteButton.superview)
        deleteButton.addSubview(centerWhiteView)
        addSubview(deleteButton)
        deleteButton.isHidden = true
        deleteButton.addTarget(self, action: #selector(handleDeletePhoto), for: .touchUpInside)
    }
    
    func presentImages(_ project: Project) {
        for img in imageViews {
            img.removeFromSuperview()
        }
        imageViews.removeAll()
        self.project = project
        print("count photo  \(self.project.photos.count)")
        // init UIIMageview -> Add subview
        project.photos.forEach { (photo) in
            let initImage = CustomImageView(image: photo.imageData)
          
            initImage.frame = photo.frame
            initImage.transform = photo.transform
            initImage.isUserInteractionEnabled = true
            initImage.addAllPhotoEffect()
            initImage.delegate = self
            
            self.addSubview(initImage)
            self.imageViews.append(initImage)
        }
        // delegate -> self
    }
    
    
    @objc func handleDeletePhoto(_ sender: UIButton) {
        guard let selected = selected else {
            return
        }
        
        project.photos.remove(at: selected)
        imageViews[selected].removeFromSuperview()
        imageViews.remove(at: selected)
        deleteButton.isHidden = true
        
        // xoa layer
        if let d = editViewDelegate {
            d.removeLayer()
        }
    }
    
    func addNewPhoto(_ image: UIImage) {
        let pickedImage = CustomImageView(image: image)
        let superViewFrame = self.frame
        pickedImage.frame = CGRect(x: 0 , y: 0, width: superViewFrame.width/2, height: superViewFrame.width/2 * (pickedImage.bounds.height)/(pickedImage.bounds.width) )
        pickedImage.center = CGPoint(x: self.frame.size.width  / 2,
                                     y: self.frame.size.height / 2)
        imageViews.append(pickedImage)
        let photo = Photo(fileName: UUID().uuidString, imageData: image, frame: CGRect(x: pickedImage.frame.origin.x, y: pickedImage.frame.origin.y, width: pickedImage.frame.width, height: pickedImage.frame.height), transform: .identity)
        project.photos.append(photo)
        self.addSubview(pickedImage)
        pickedImage.isUserInteractionEnabled = true
        pickedImage.addAllPhotoEffect()
        pickedImage.delegate = self
    }
    
    func saveProject() {
        project.saveProject()
    }
}

extension GalleryView: CustomImageViewDelegate {
    func imageDidChange(_ view: CustomImageView) {
        //        guard let i = selected else {return}
        guard let i = imageViews.firstIndex(of: view) else {return}
        print("selected  \(i)")
        // images[i]
        // imageVIews[i]
        if let d = galleryViewDelegate {
            d.photoSelected(view)
        }
        project.photos[i].save(view)
    }
    
    func imageDidSelect(_ view: CustomImageView) {
        guard let i = imageViews.firstIndex(of: view) else {return}
        selected = i
        
        bringSubviewToFront(view)
        let radian:Double = atan2( Double(view.transform.b), Double(view.transform.a))
        let imageTransform = view.transform
        let scale = sqrt(Double(imageTransform.a * imageTransform.a + imageTransform.c * imageTransform.c))
        
        let midPoint = view.convert(CGPoint(x: view.bounds.midX, y: -15/scale), to: self)
        
        deleteButton.transform = .identity
        deleteButton.frame = CGRect(x: midPoint.x - 10, y: midPoint.y - 10, width: 20, height: 20)
        deleteButton.transform = CGAffineTransform(rotationAngle: CGFloat(radian))
        deleteButton.isHidden = false
        bringSubviewToFront(deleteButton)
        if let d = galleryViewDelegate {
            d.photoSelected(view)
        }
    }
}


