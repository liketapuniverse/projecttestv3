//
//  EditView.swift
//  ProjectTestV3
//
//  Created by admin on 14/10/2021.
//
import UIKit
protocol EditViewDelegate: AnyObject {
    func removeLayer()
}

class EditView: UIView {
    var borderLayer = CAShapeLayer()
    var circleShapeLayer = CAShapeLayer()
    
    func drawForView(_ view: UIView) {
        // add border
        isHidden = false
        // draw
        
        let topLeft = view.convert(CGPoint(x: 0, y: 0), to: self)
        let topRight = view.convert(CGPoint(x: view.bounds.maxX, y: 0), to: self)
        let bottomLeft = view.convert(CGPoint(x: 0, y: view.bounds.maxY), to: self)
        let bottomRight = view.convert(CGPoint(x: view.bounds.maxX, y: view.bounds.maxY), to: self)
        let rectBorderPath = UIBezierPath()
        
        rectBorderPath.move(to: topLeft)
        rectBorderPath.addLine(to: topRight)
        rectBorderPath.addLine(to: bottomRight)
        rectBorderPath.addLine(to: bottomLeft)
        rectBorderPath.addLine(to: topLeft)
        
        borderLayer.path = rectBorderPath.cgPath
        
        borderLayer.fillColor = UIColor.clear.cgColor
        borderLayer.strokeColor = UIColor.blue.cgColor
        borderLayer.lineWidth = 3
        
        let path = UIBezierPath()
        path.append(UIBezierPath(arcCenter: CGPoint(x: topLeft.x, y: topLeft.y), radius: 8, startAngle: 0, endAngle: .pi*2, clockwise: true))
        path.append(UIBezierPath(arcCenter: CGPoint(x: topRight.x, y: topRight.y), radius: 8, startAngle: 0, endAngle: .pi*2, clockwise: true))
        path.append(UIBezierPath(arcCenter: CGPoint(x: bottomLeft.x, y: bottomLeft.y), radius: 8, startAngle: 0, endAngle: .pi*2, clockwise: true))
        path.append(UIBezierPath(arcCenter: CGPoint(x: bottomRight.x, y: bottomRight.y), radius: 8, startAngle: 0, endAngle: .pi*2, clockwise: true))
        
        circleShapeLayer.path = path.cgPath
        circleShapeLayer.setFillAndStrokeColor(color: .blue)
        addLayers()
    }
    
    func addLayers() {
        layer.addSublayer(borderLayer)
        layer.addSublayer(circleShapeLayer)
    }
}

extension CAShapeLayer {
    func makeCirclePath(ordinate: CGRect) {
        self.path = UIBezierPath(ovalIn: ordinate).cgPath
        self.setFillAndStrokeColor(color: UIColor.blue)
    }
    
    func setFillAndStrokeColor(color: UIColor?) {
        self.fillColor = color?.cgColor
        self.strokeColor = color?.cgColor
    }
}

extension EditView: EditViewDelegate {
    func removeLayer() {
        self.layer.sublayers?.forEach({ layer in
            layer.removeFromSuperlayer()
        })
    }
    
    
}
