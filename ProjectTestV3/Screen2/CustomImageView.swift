import UIKit

// protocol

protocol CustomImageViewDelegate: AnyObject {
    func imageDidChange(_ view: CustomImageView)
    func imageDidSelect(_ view: CustomImageView)
}

class CustomImageView: UIImageView {
    
    // add gesture
    // call protocol when changed
    
    var lastRotation: CGFloat = 0
    var originalRotation = CGFloat()
    var pinchCenter:CGPoint = .zero
    var lastScale: CGFloat = 0
    var minScale: CGFloat = 0.5
    var maxScale: CGFloat = 3
    
    weak var delegate: CustomImageViewDelegate?
    
    func notifyChange() {
        if let d = delegate {
            d.imageDidChange(self)
            d.imageDidSelect(self)
        }
    }
    
    func addAllPhotoEffect() {
        addTapGesture()
        addPinchGesture()
        addPanGesture()
        addRotateGesture()
    }
    
    @objc func rotatedView(_ sender: UIRotationGestureRecognizer) {
        if sender.state == .began {
            sender.rotation = lastRotation
            originalRotation = sender.rotation
        } else if sender.state == .changed {
            self.transform = self.transform.rotated(by: sender.rotation)
            sender.rotation = 0
        } else if sender.state == .ended {
            print("end")
        }
        notifyChange()
    }
    
    @objc func handlePanGesture(gesture: UIPanGestureRecognizer) {
        let transform = self.transform
        
        // Initialize imageView.transform
        self.transform = CGAffineTransform.identity
        
        // Move UIImageView
        let point: CGPoint = gesture.translation(in: self)
        let movedPoint = CGPoint(x: self.center.x + point.x,
                                 y: self.center.y + point.y)
        self.center = movedPoint
        
        // Revert imageView.transform
        self.transform = transform
        
        // Reset translation
        gesture.setTranslation(CGPoint.zero, in: self)
        notifyChange()
    }
    
    @objc func pinchGessture(sender: UIPinchGestureRecognizer) {
        if sender.state == UIGestureRecognizer.State.began { // Begin pinch
            let touchPoint1 = sender.location(ofTouch: 0, in: self)
            let touchPoint2 = sender.location(ofTouch: 1, in: self)
            
            // Get mid point of two touch
            self.pinchCenter = CGPoint(x: (touchPoint1.x + touchPoint2.x) / 2, y: (touchPoint1.y + touchPoint2.y) / 2)
            lastScale = sender.scale
            
        } else if sender.state == UIGestureRecognizer.State.changed { // Pinching in operating
            // Store scale
            
            let pinchCenter = CGPoint(x: sender.location(in: self).x - self.bounds.midX,
                                      y: sender.location(in: self).y - self.bounds.midY)
            let transform = self.transform.translatedBy(x: pinchCenter.x, y: pinchCenter.y)
                .scaledBy(x: sender.scale, y: sender.scale)
                .translatedBy(x: -pinchCenter.x, y: -pinchCenter.y)
            self.transform = transform
            sender.scale = 1
            
        }
        if sender.state == UIGestureRecognizer.State.ended { // End pinch
            let currentScale = sqrt(abs(self.transform.a * self.transform.d - self.transform.b * self.transform.c))
            if currentScale <= self.minScale { // Under lower scale limit
                UIView.animate(withDuration: 0.2, delay: 0.0, options: UIView.AnimationOptions.curveEaseOut, animations: {() -> Void in
                    self.transform = CGAffineTransform(scaleX: self.minScale, y: self.minScale)
                }, completion: {(finished: Bool) -> Void in
                })
            } else if self.maxScale <= currentScale { // Upper higher scale limit
                UIView.animate(withDuration: 0.2, delay: 0.0, options: UIView.AnimationOptions.curveEaseOut, animations: {() -> Void in
                    self.transform = CGAffineTransform(scaleX: self.maxScale, y: self.maxScale)
                    
                }, completion: {(finished: Bool) -> Void in
                })
            }
        }
        notifyChange()
    }
    
    @objc func handleTap() {
        notifyChange()
    }
    func addPanGesture() {
        self.addGestureRecognizer(UIPanGestureRecognizer(target: self, action: #selector(self.handlePanGesture)))
    }
    
    func addPinchGesture() {
        let pinchGesture = UIPinchGestureRecognizer(target: self, action: #selector(self.pinchGessture))
        self.addGestureRecognizer(pinchGesture)
    }
    
    func addRotateGesture() {
        let rotate = UIRotationGestureRecognizer(target: self, action:     #selector(self.rotatedView(_:)))
        self.addGestureRecognizer(rotate)
    }
    
    func addTapGesture() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.handleTap))
        self.addGestureRecognizer(tapGesture)
    }
}
