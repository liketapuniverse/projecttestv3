//
//  Screen2ViewController.swift
//  ProjectTestV3
//
//  Created by admin on 14/10/2021.
//

import UIKit

class Screen2ViewController: UIViewController {
    var project: Project?
    var model = ProjectsData.shared
    var imagePicker = UIImagePickerController()
    @IBOutlet weak var galleryView: GalleryView!
    @IBOutlet weak var editView: EditView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Back", style: .plain, target: self, action: #selector(handleBackButton))
        self.navigationItem.leftBarButtonItem?.isEnabled = true
        galleryView.galleryViewDelegate = self
        galleryView.editViewDelegate = editView // remove layers
        guard let project = project else {
            return
        }
        title = project.name

        project.getProjectDetail({
            self.galleryView.presentImages(project)
        })
    }
    
    @objc func handleBackButton() {
        self.showAlertView(title: "Save change", message: "Do you want to save changes to gallery?", firstButton: "Save", otherButtons: ["Don't save"], type: .alert) {
            // save
           // truyen sang gallery save
            self.galleryView.saveProject()
            self.navigationController?.popViewController(animated: true)
//            NotificationCenter.default.addObserver(forName: NSNotification.Name("didFinishSavingProject"), object: nil, queue: nil) { _ in
//                self.navigationController?.popViewController(animated: true)
//            }
           
        } otherAction: { (_) in
            print("don't save")
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func addPhoto(_ sender: Any) {
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
            imagePicker.delegate = self
            imagePicker.sourceType = .savedPhotosAlbum
            imagePicker.allowsEditing = false
            present(imagePicker, animated: true, completion: nil)
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}

extension Screen2ViewController: GalleryViewDelegate {
    func photoSelected(_ view: UIView) {
        editView.drawForView(view)
    }
}

extension Screen2ViewController: UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            galleryView.addNewPhoto(image)
        }
        picker.dismiss(animated: true, completion: nil);
    }
}
