//
//  ProjectData.swift
//  ProjectTestV3
//
//  Created by admin on 15/10/2021.
//

import UIKit
import Alamofire
import SVProgressHUD

class Project {
    let name: String
    let id: String
    var photos: [Photo] = []
    var didCache = false
    let url = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
    
    
    init(id: String, name: String) {
        self.id = id
        self.name = name
    }
    
    func getProjectDetail(_ completion: (() -> Void)? = nil) {
        // clear old photo
        photos = []
        // check exist in cache
        if UserDefaults.standard.bool(forKey: "didCache-\(self.id)") {
            guard let listFrame = getListFrame() else {
                return
            }
            for photo in listFrame {
                if photo.projectId == self.id {
                    guard let toAddPhoto = Photo.load(fileName: photo.imageId,photo: photo) else { continue}
                    self.photos.append(toAddPhoto)
                }
            }
            completion!()
        } else {
            SVProgressHUD.show()
            let parameters: [String: Any] = ["id": self.id]
            AF.request("\(AppConstants.endPoint)/xprojectdetail", method: .post, parameters: parameters).responseJSON { response in
                guard
                    let json = response.value as? [String:Any],
                    let resPhotos = json["photos"] as? [[String: Any]]
                else {
                    SVProgressHUD.dismiss()
                    return
                }
                for i in 0..<resPhotos.count {
                    guard
                        let url = resPhotos[i]["url"] as? String,
                        let frame = resPhotos[i]["frame"] as? [String:Any],
                        let x = frame["x"] as? Int,
                        let y = frame["y"] as? Int,
                        let width = frame["width"] as? Int,
                        let height = frame["height"] as? Int
                    else {
                        return
                    }
                    // done get data
                    let imageURL = URL(string: url)
                    var image: UIImage?
                    if let imageURL = imageURL {
                        let imageData = NSData(contentsOf: imageURL)
                        if imageData != nil {
                            image = UIImage(data: imageData! as Data)
                            guard let image = image else {
                                return
                            }
                            let photo = Photo(fileName: UUID().uuidString, imageData: image, frame: CGRect(x: x, y: y, width: width, height: height), transform: .identity )
                            self.photos.append(photo)
                        }
                    }

                }
                if completion != nil {
                    completion!()
                }
                SVProgressHUD.dismiss()
            }
        }
    }
    
    func saveProject() {
        var listFrame: [PhotoFrameData] = []
        for i in 0..<photos.count {
            let photo = photos[i]
            // minX ?
            let photoFrame = PhotoFrameData(imageId: photo.fileName, projectId: self.id,rect: photo.frame, transform: photo.transform)
            listFrame.append(photoFrame)
            let fileURL = self.url.appendingPathComponent("\(photo.fileName).jpg")
            if let data = photo.imageData.jpegData(compressionQuality:  1.0),
              !FileManager.default.fileExists(atPath: fileURL.path) {
                do {
                    try data.write(to: fileURL)
                    print("file saved--\(fileURL.path)")
                } catch {
                    print("error saving file:", error)
                }
            }
        }
        guard let data = try? JSONEncoder().encode(listFrame) else { return  }
        let encodedString = String(data: data, encoding: .utf8)!
        print("encode \(encodedString)")
        
        if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            let fileURL = dir.appendingPathComponent("listFrame\(self.id).json")
            do {
                try encodedString.write(to: fileURL, atomically: false, encoding: .utf8)
            }
            catch let error {
                print(error)
            }
        }
        
        UserDefaults.standard.set(true, forKey:"didCache-\(self.id)")
        
    }
    
    func getListFrame() -> [PhotoFrameData]? {
        if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            let fileURL = dir.appendingPathComponent("listFrame\(self.id).json")
            if let jsonString = try? String(contentsOf: fileURL, encoding: .utf8) {
                if let dataFromString = jsonString.data(using: .utf8, allowLossyConversion: false) {
                    do {
                        let listFrame = try JSONDecoder().decode([PhotoFrameData].self, from: dataFromString)
                        return listFrame

                    } catch {
                        print(error)
                        return nil
                    }
                }
            }
        }
        return nil
    }
}

class Photo {
    var fileName: String
    let imageData: UIImage
    var frame = CGRect.zero
    var transform = CGAffineTransform.identity  // default 1 , 0 , 0, 1, 0, 0
    init(fileName: String, imageData: UIImage, frame: CGRect, transform: CGAffineTransform) {
        self.fileName = fileName
        self.imageData = imageData
        self.frame = frame
        self.transform = transform
    }
    static func load(fileName: String, photo: PhotoFrameData) -> Photo?{
        let url = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        let fileURL = url.appendingPathComponent("\(fileName).jpg")
        
        guard let image = UIImage(contentsOfFile: fileURL.path) else { return nil
        }
        return Photo(fileName: photo.imageId, imageData: image, frame: photo.rect, transform: photo.transform)
    }
    func save(_ view: UIImageView) {
        let t = view.transform
        transform = CGAffineTransform(a: t.a, b: t.b, c: t.c, d: t.d, tx: t.tx, ty: t.ty)
        view.transform = .identity
        frame = CGRect(x: view.frame.minX, y: view.frame.minY, width: view.frame.width, height: view.frame.height)
        view.transform = t
    }
    
}

class PhotoFrameData: Codable {
    let imageId, projectId: String
    let rect: CGRect
    let transform: CGAffineTransform
    init(imageId:  String, projectId: String, rect: CGRect,transform: CGAffineTransform) {
        self.imageId = imageId
        self.projectId = projectId
        self.rect = rect
        self.transform = transform
    }
}
