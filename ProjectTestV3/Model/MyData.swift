//
//  MyData.swift
//  ProjectTestV3
//
//  Created by admin on 14/10/2021.
//

import Foundation
import Alamofire
import SVProgressHUD

class ProjectsData {
    static let shared = ProjectsData()
    var projects : [Project] = []
    let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
    
    init() {
        getProjectsList()
    }
    
    func getProjectsList() {
        // get from UserDefault // id list + name
        if let listCodableProjects = readSavedProjectsList() {
            self.projects = listCodableProjects.map({ codableProject in
                Project(id: codableProject.id, name: codableProject.name)
            })
            NotificationCenter.default.post(name: NSNotification.Name("didFinishGetListProject"), object: nil)
            return
        }
        SVProgressHUD.show()
        AF.request("\(AppConstants.endPoint)/xproject", method: .get).responseJSON { response in
            guard
                let json = response.value as? [String:Any],
                let pros = json["projects"] as? [[String: Any]]
            else {
                return
            }
            for i in 0..<pros.count {
                guard
                    let id = pros[i]["id"] as? Int,
                    let name = pros[i]["name"] as? String
                else {
                    return
                }
                let project = Project(id: String(id), name: name)
                self.projects.append(project)
            }
            SVProgressHUD.dismiss()
            NotificationCenter.default.post(name: NSNotification.Name("didFinishGetListProject"), object: nil)
        }
        
    }
    
    func saveProjectsList() {
        let listCodableProjects = self.projects.map { project in
            CodableProject(id: project.id, name: project.name)
        }
        guard let data = try? JSONEncoder().encode(listCodableProjects) else { return  }
        let encodedString = String(data: data, encoding: .utf8)!
        print("encode \(encodedString)")
        
        if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            let fileURL = dir.appendingPathComponent(AppConstants.projectsListFileName)
            do {
                try encodedString.write(to: fileURL, atomically: false, encoding: .utf8)
            }
            catch let error {
                print(error)
            }
        }
    }
    
    func readSavedProjectsList() -> [CodableProject]? {
        guard let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else { return nil}
        let fileURL = dir.appendingPathComponent(AppConstants.projectsListFileName)
        guard let jsonString = try? String(contentsOf: fileURL, encoding: .utf8),
              let dataFromString = jsonString.data(using: .utf8, allowLossyConversion: false),
              let projectsList = try? JSONDecoder().decode([CodableProject].self, from: dataFromString) else { return nil}
        return projectsList
    }
    
}

class CodableProject: Codable {
    let id: String
    let name: String
    init(id: String, name: String) {
        self.id = id
        self.name = name
    }
}
